
terraform {
  required_version = ">= 0.13"
  required_providers {
    digitalocean = {
      source = "terraform-providers/digitalocean"
    }
    local = {
      source = "hashicorp/local"
    }
    template = {
      source = "hashicorp/template"
    }
  }
}
