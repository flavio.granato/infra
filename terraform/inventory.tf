data "template_file" "inventory" {
  template = file("./inventory.tpl")

  vars = {
    front-proxy_pub_ip = digitalocean_droplet.front-proxy.ipv4_address
    master_pub_ip      = digitalocean_droplet.master.ipv4_address
    master_name        = digitalocean_droplet.master.name
    workers_pub_ip = join(
      "\n",
      formatlist(
        "            %s:",
        digitalocean_droplet.workers.*.ipv4_address,
      ),
    )
  }
}

resource "local_file" "save_inventory" {
  content  = data.template_file.inventory.rendered
  filename = "../ansible/hosts"
}

