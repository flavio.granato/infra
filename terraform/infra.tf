resource "digitalocean_ssh_key" "default" {
  name       = "Terraform+kubernetes"
  public_key = file("~/.ssh/terraform-id_ed25519.pub")
}

resource "digitalocean_droplet" "front-proxy" {
  image              = "debian-10-x64"
  name               = "front-proxy"
  region             = "nyc3"
  size               = "1gb"
  private_networking = "true"
  ssh_keys           = [digitalocean_ssh_key.default.fingerprint]
}

resource "digitalocean_domain" "default" {
  name = "flaviogranato.com.br"
}

resource "digitalocean_record" "traefik" {
  domain = digitalocean_domain.default.name
  type   = "A"
  name   = "traefik"
  value  = digitalocean_droplet.front-proxy.ipv4_address
}

resource "digitalocean_record" "linkerd" {
  domain = digitalocean_domain.default.name
  type   = "A"
  name   = "dashboard"
  value  = digitalocean_droplet.front-proxy.ipv4_address
}


resource "digitalocean_record" "gitea" {
  domain = digitalocean_domain.default.name
  type   = "A"
  name   = "gitea"
  value  = digitalocean_droplet.front-proxy.ipv4_address
}

resource "digitalocean_record" "gocd" {
  domain = digitalocean_domain.default.name
  type   = "A"
  name   = "gocd"
  value  = digitalocean_droplet.front-proxy.ipv4_address
}

resource "digitalocean_droplet" "master" {
  image              = "debian-10-x64"
  name               = "master"
  region             = "nyc3"
  size               = "4gb"
  private_networking = "true"
  ssh_keys           = [digitalocean_ssh_key.default.fingerprint]
}

resource "digitalocean_droplet" "workers" {
  count              = 2
  image              = "debian-10-x64"
  name               = "worker-0${count.index + 1}"
  region             = "nyc3"
  size               = "4gb"
  private_networking = "true"
  ssh_keys           = [digitalocean_ssh_key.default.fingerprint]
}

output "controller_ip_address" {
  value = digitalocean_droplet.master.ipv4_address
}

output "controller_private_ip_address" {
  value = digitalocean_droplet.master.ipv4_address_private
}

output "node_ip_address" {
  value = digitalocean_droplet.workers.*.ipv4_address
}

output "node_private_ip_address" {
  value = digitalocean_droplet.workers.*.ipv4_address_private
}

output "node_front-proxy_ip_address" {
  value = digitalocean_droplet.front-proxy.ipv4_address_private
}

