# ansible-configured
all:
  vars:
    ansible_ssh_user: root
    private_key_file: ./terraform-id_ed25519
    cert_domain_name: www.flaviogranato.com.br
    cert_admin_email: flavio.granato@protonmail.com
  children:
    front-proxy:
      hosts:
        ${front-proxy_pub_ip}:
    kubernetes:
      children:
        master:
          vars:
            linkerd_url: linkerd.flaviogranato.com.br
          hosts:
            ${master_pub_ip}:
        workers:
          hosts:
${workers_pub_ip}
